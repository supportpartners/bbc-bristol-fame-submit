Readme
======

App written in NodeJS


Variables:
-----------
Paths are stored in the 'main.js' file located in the js folder when you show contents -> FAME Submit.app/Contents/Resources/app.nw/js/main.js

These are the ones that most likely need to be changed:

1. var ameXMLFileLocation = "/Volumes/BPVMCR/07_FPL_PROCESSING/02_FPL_POC_FILES/02_XML/SUBMIT_OUTPUT/”; 
- This is the parent location of where the XML files will be written by Submit. This directory MUST exist before the app is started. 
// Shared location for XML files - assuming workflow is that they will be manually dragged into INBOX watchfolder for FAME-Render to process
// could be set to write directly into INBOX Watchfolder for instant processing by FAME-Render if desired


2. var logFile = "/Users/Shared/Logs/" + appName + "/log_" + mo + ".txt"
- This is the log file :) If you need to change this, make sure to only change the bit at the beginning (before " + appName) only.

3. var verboseLogging = true
- This outputs a lot of info to the log file and the screen. Probably useful, but included as an option if you want less noise

4. var reportingDir = "/Volumes/BPVMCR/07_FPL_PROCESSING/02_FPL_POC_FILES/01_JSON/"
This is where the JSON files go for each reel - for the future Dashboard

What does Submit do?!
----------------------

User selects the ‘Job’ folder. This is the parent that will contain the ‘UNPACKED’ and ‘WORKING’ directories.
The app will then show the reels that are in the Job in a list and shows the location it will be output to (this uses the parent folder location of ameXMLFileLocation).
User selects ‘Create XML File(s)’ and this will output the XMLs to the specified location.


Version release notes:
v1 September, written by Jemma, called FAME Tyrone
v2 18 November, added support for R3D files, renamed from Tyrone to Submit
v3 25 November, removed SOURCE folder, changed proxies folder to WEBPROXY, added styling to appmessage to make it bigger, added line to skip .DS_Store files




Example XML:
<xml>
  <reel>
    <file>
      <jobName>PROD-SORT_JOBID_DRIVENo</jobName>
      <reelName>DIYSOS-S28_20170418_XD_001</reelName>
      <fileName>/FPL POC/New/PROD-SORT_JOBID_DRIVENo/UNPACKED/DIYSOS-S28_20170418_XD_001/SOURCE/DIYSOS-S28_20170418_XD_001_PS_ 1001.MXF</fileName>
      <proxy_destination>/FPL POC/New/PROD-SORT_JOBID_DRIVENo/UNPACKED/DIYSOS-S28_20170418_XD_001/PROXY</proxy_destination>
      <thumbnail_destination>/FPL POC/New/PROD-SORT_JOBID_DRIVENo/UNPACKED/DIYSOS-S28_20170418_XD_001/THUMBNAIL</thumbnail_destination>
      <proxy_complete>false</proxy_complete>
      <thumbnail_complete>false</thumbnail_complete>
      <ame_processedBy></ame_processedBy>
      <message/>
    </file>
  </reel>
</xml>
