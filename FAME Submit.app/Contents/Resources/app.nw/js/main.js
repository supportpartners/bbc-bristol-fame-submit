//this must exist
var ameXMLFileLocation = "/Volumes/BPVMCR/07_FPL_PROCESSING/02_FPL_POC_FILES/02_XML/SUBMIT_OUTPUT/"; 
// Shared location for XML files - assuming workflow is that they will be manually dragged into INBOX watchfolder for FAME-Render to process
// could be set to write directly into INBOX Watchfolder for instant processing by FAME-Render if desired



var strFileList;
var arrReels = [];
var arrReelsFullPath = [];
var strJobName = '';
var strFileListRoot = '';
var fs = require('fs');
var fuj= true;
var fur = false;

var strJobFolderParent = '';
var strNewJobFolderParent = '';
//var unpackDest = '';
var reelLocation = '';
var newDestinationPicked = false;

var appName = "FAME-Submit";


var ameXMLFile = "";

var mo = require('moment');
mo = mo().format('YYMMDD');

var logFile = "/Users/Shared/Logs/FAME-Submit/log_" + mo + ".txt";
var logParent = "/Users/Shared/Logs/";

var reportingDir = "/Volumes/BPVMCR/07_FPL_PROCESSING/02_FPL_POC_FILES/01_JSON/"

var verboseLogging = true;

var os = require('os');
var machineName = os.hostname();

document.onreadystatechange = function () {
  var state = document.readyState;
  if (state == 'interactive') {
     // init();
  } else if (state == 'complete') {
  
	  initOnCompleteLoad();
	  
	  writeLogToScreen("Welcome to FAME Submit!", 0);
	  
  }
}

	function writeLogToScreen(strMessage, level){
		try{
			if (verboseLogging == true || level == 1){
				var now = require('moment');
				now = now().format('YY/MM/DD HH:mm:ss');
			
				document.getElementById('appMessages').innerHTML = strMessage;
				writeLog(now + ": " + strMessage + "\n");
			}
		}catch(e){
			document.getElementById('appMessages').innerHTML = "Logging error: " + e.message;
		}
	}
	
	function writeLog(strMessage){
		if (!fs.existsSync(logParent)) {	
			fs.mkdirSync(logParent, err => {
    			if (err && err.code != 'EEXIST') throw err	
			});
		}
		
	  	if (!fs.existsSync(logParent + appName + "/")) {	
			fs.mkdirSync(logParent + appName + "/", err => {
    			if (err && err.code != 'EEXIST') throw err	
			});
		}
	
		
		fs.appendFileSync(logFile, strMessage);
	}


function initOnCompleteLoad(){
try{
	document.getElementById("open").addEventListener('change', function (e) {
		
		strFileList;
		arrReels = [];
		arrReelsFullPath = [];
		strJobName = '';
		strFileListRoot = '';
		strJobFolderParent = '';
		strNewJobFolderParent = '';
		reelLocation = '';
		newDestinationPicked = false;
	
		writeLogToScreen("User selected a new job folder", 0);
		
		strFileList = e.target.files;
		
		if (strFileList.length > 0){
			writeLogToScreen(strFileList.length + " files found within that structure", 0);
			//if (strFileList[0].name.toLowerCase() != '.ds_store'){
				//writeLogToScreen("");
				//strFileList[i].name = name;
				//strFileList[0].webkitRelativePath = path after the selected directory name
				var strFolderPath = this.value.split(";")[0];
				var strFileRelPath = '';
				
				for (var i=0; i < strFileList.length; i++){
					if (!strFileList[i].name.endsWith(".DS_Store")){
						strFileRelPath = strFileList[i].webkitRelativePath;
						writeLogToScreen("Relative file path to find job name from: " + strFileRelPath, 0);
						strFolderPath = this.value.split(";")[i];
						writeLogToScreen("Folder path: " + strFolderPath, 0);
						i = strFileList.length + 10;
					}
				}
				
				if (strFileList != ""){
					//strJobName = strFolderPath.replace(strFileRelPath, '');
					var folderNameSplit = strFileRelPath.split("/");
					strJobName = folderNameSplit[0];
					
					writeLogToScreen("Job name: " + strJobName, 0);
				
					var fileListRootTemp = strFolderPath.replace(strFileRelPath, '');
					reelLocation = fileListRootTemp + strJobName + '/UNPACKED';
					strJobFolderParent = reelLocation;
					
					writeLogToScreen("Reel  files to: " + reelLocation, 0);
					
					populateJobName();
					
					writeLogToScreen("Initial stage complete.", 0);
				}else{
					writeLogToScreen("Only '.DS_Store' files found. Please check directory and try again", 1);
				}
								
			//}else{
			//	writeLogToScreen("No files found, check directory and try again");
			//}
		}else{
			writeLogToScreen("No files found, check directory and try again", 1);
		}
		
	});
	}catch(e){
		writeLogToScreen("Error on startup", 1);
	}
}

function populateJobName(){
	document.getElementById('dStep2').style = "visibility: visible";

	document.getElementById('jobName').innerHTML = strJobName;

	getReels();
}

function getReels(){
	var filePath = '';
	var ul = document.getElementById("reels");
	ul.innerHTML = "";
	writeLogToScreen("Cleared reel display value from screen", 0);
	
	for (var i=0; i < strFileList.length; i++){
		for (var i=0; i < strFileList.length; i++){
			writeLogToScreen("Checking if filename starts with a . and if so, ignore", 0);
			if (!strFileList[i].name.endsWith(".DS_Store")){
			
				filePath = strFileList[i].webkitRelativePath;
				
				var filePathSplit = filePath.split("/");
				
				if (filePathSplit[1] == 'UNPACKED'){
					var reelName = filePathSplit[2];
					var reelPath = filePathSplit[0] + '/' + filePathSplit[1] + '/' + filePathSplit[2];
				
					writeLogToScreen("Check if reel already added to array, if so, ignore: " + reelName, 0);
					if (arrReels.indexOf(reelName) < 0){
						writeLogToScreen("Adding reel full path to array: " + reelPath, 0);
						arrReelsFullPath.push(reelPath);
				
						let item = document.createElement("li");
						item.innerHTML = reelName;
						writeLogToScreen("Adding reel name to display: " + reelName, 0);
						document.getElementById("reels").appendChild(item);
						writeLogToScreen("Adding reel name to reel array: " + reelName, 0);
						arrReels.push(reelName);
					}
				}
			}
		}
	}
	
//	if (!fs.existsSync(ameXMLFileLocation + "/INBOX/")) {	
//			fs.mkdirSync(ameXMLFileLocation + "/INBOX/", err => {
//    			if (err && err.code != 'EEXIST') throw err	
//			});
//		}
		
	ameXMLFile =  ameXMLFileLocation + strJobName;
	
	document.getElementById('pLocation').innerHTML = "Output location will be to " + ameXMLFile + "_*reel name*.xml";
}



function doAMEXMLFile(){
		writeLogToScreen("Writing reel details out to xml file", 0);
		
		
		var sPrevFileName = "";
		var strJobMessage = "";
		var strJobMessageReason = "";
		var strReelMessage = ""; 
		var strReelMessageReason = "";
		var strFileMessage = "";
		var strFileMessageReason = "";
						
		try{
			writeLogToScreen("Check if file list contains files", 0);
			if (strFileList != undefined){
				if (strFileList.length > 0){
					
					for (var i=0; i < arrReels.length; i++){
						var reelName = arrReels[i];
						
						writeLogToScreen("Reel: " + reelName, 0);
							
						var strAMEText = "<xml>";
						strAMEText += "<reel>"
						
						
			 			var blnJobStop = false;
						var blnReelStart = false;
						var blnReelStop = false;
						var blnFileStop = false;
						
						var updatePrev = false;
							
//						fs.readdirSync(strJobFolderParent + "/" + reelName + "/SOURCE").forEach(file => {
//							var sFileName = strJobFolderParent + "/" + reelName + "/SOURCE/" + file;
							
						fs.readdirSync(strJobFolderParent + "/" + reelName).forEach(file => {
							var sFileName = strJobFolderParent + "/" + reelName + "/" + file;
							
							if (!sFileName.endsWith(".DS_Store")){
						
									if (processingJobName != strJobName){
										if (processingJobName != ""){
											blnJobStop = true; //stop previous job
											updatePrev = true;
										}
									}
								
									// This is the new bit that adds the path to the R3D file instead of the RDC file for Red reels - Rupert
									if (sFileName.endsWith(".RDC")){
											sFileNameSplit = sFileName.split("/");
											finalChunkOfFileNameSplit = sFileNameSplit.length-1
											sFileName = sFileName + "/" + sFileNameSplit[finalChunkOfFileNameSplit].slice(0,-4) +"_001.R3D"
																		
									}
						
									if (processingReelName != reelName){
										if (processingReelName != ""){
											blnReelStop = true; //stop previous reel
											updatePrev = true;
										}
									}
								
									if (sPrevFileName != sFileName){
										if (sPrevFileName != ""){
											updatePrev = true;
										}
									}
								
									if (updatePrev == true){
										writeJSONFile(sPrevFileName, blnJobStop, blnReelStart, false, blnReelStop, true, strJobMessage, strJobMessageReason, strReelMessage, strReelMessageReason, strFileMessage, strFileMessageReason);
									}else{
										sPrevFileName = sFileName;
									}
								
								
					
						
									strAMEText += "<file>" + 
													"<jobName>" + strJobName + "</jobName>" + 
													"<reelName>" + reelName + "</reelName>" + 
													"<fileName>" + sFileName + "</fileName>" + 
	//    											"<proxy_destination>" + strJobFolderParent + "/" + reelName + "/SOURCE/proxies</proxy_destination>" +
	//    											"<thumbnail_destination>" + strJobFolderParent + "/" + reelName + "/SOURCE/proxies</thumbnail_destination>" +
													"<proxy_destination>" + strJobFolderParent + "/" + reelName + "/proxies</proxy_destination>" +
													"<thumbnail_destination>" + strJobFolderParent + "/" + reelName + "/proxies</thumbnail_destination>" +
													"<proxy_complete>false</proxy_complete>" +
													"<thumbnail_complete>false</thumbnail_complete>" +
													"<ame_proccessedBy></ame_proccessedBy>" +
													"<message></message>" +
													"</file>";
								
								
									processingReelName = reelName;
									processingJobName = strJobName;
								
									strFileMessage = "Found file to add to XML";
									strFileMessageReason = "INFO";
								
									sPrevFileName = sFileName;
								
									blnJobStop = false;
									blnReelStart = false;
									blnReelStop = false;
									blnFileStop = false;
									updatePrev = false;
								
									writeJSONFile(sFileName, blnJobStop, blnReelStart, true, blnReelStop, true, strJobMessage, strJobMessageReason, strReelMessage, strReelMessageReason, strFileMessage, strFileMessageReason);
							
								}
  						});
						
							
						strAMEText += "</reel>";
						strAMEText += "</xml>";
						
						
							
						if (!fs.existsSync(ameXMLFileLocation)) {	
							fs.mkdirSync(ameXMLFileLocation, err => {
    							if (err && err.code != 'EEXIST') throw err	
							});
						}
		
						fs.appendFileSync(ameXMLFile + "_" + reelName + ".xml", strAMEText);
						
						strReelMessage = "Reel written to XML";
						strReelMessageReason = "INFO";
						
						writeJSONFile(sPrevFileName, false, false, false, true, false, strJobMessage, strJobMessageReason, strReelMessage, strReelMessageReason, strFileMessage, strFileMessageReason);
							
					}
					
					strJobMessage = "All reels for job written out";
					strJobMessageReason = "INFO";
					
					writeJSONFile(sPrevFileName, true, false, false, false, false, strJobMessage, strJobMessageReason, strReelMessage, strReelMessageReason, strFileMessage, strFileMessageReason);
					
					
					writeLogToScreen('XML Files created - have a great day!', 1);
				}else{
					writeLogToScreen('Please select a folder before continuing', 1);	
				}
			}else{
				writeLogToScreen('Please select a folder before continuing', 1);	
			}
		}catch(e){
			writeLogToScreen(e.message, 1);
			
			if (sPrevFileName != ""){
				strJobMessage = "Error written reel and job data out to XML: " + e.message;
				strReelMessage = strJobMessage;
				strFileMessage = strJobMessage;
				
				strJobMessageReason = "ERROR";
				strReelMessageReason = strJobMessageReason;
				strFileMessageReason = strJobMessageReason;
			
				writeJSONFile(sPrevFileName, true, false, false, true, true, strJobMessage, strJobMessageReason, strReelMessage, strReelMessageReason, strFileMessage, strFileMessageReason);
			}
					
		}		
}


var processingReelName = "";
var processingJobName = "";

function writeJSONFile(strFileName, blnJobStop, blnReelStart, blnFileStart, blnReelStop, blnFileStop, strJobMessage, strJobMessageReason, strReelMessage, strReelMessageReason, strFileMessage, strFileMessageReason){
	var jsonFileName = reportingDir + "/" + processingJobName + ".json";
	
	if (processingJobName == ""){
		jsonFileName = reportingDir + "/" + arrProxyJobs[0].job + ".json";
		processingJobName = arrProxyJobs[0].job;
	}
	if (processingReelName == ""){
		processingReelName = arrProxyJobs[0].reel;
	}
	
	var existingJSON = null;
	
	var intApp = -1;
	var intReel = -1;
	var intFile = -1;
	var intJob = -1;
	
	var strProdCode = productionCode(processingJobName);
	var strCameraCode = cameraCode(processingReelName);
	var strFileSize = "";
	var strDuration = "";
	var strCodec = "";
	var strHeight = "";
	var strWidth = "";
	var strFPS = "";
	var strAspectRatio = "";
	
	try{
		var data = fs.readFileSync(jsonFileName);
			
		existingJSON = JSON.parse(data);
	}catch(e){
		writeLogToScreen("Unable to read job file - caught: " + e.message, 1);
	}
	
	try{
	var now = require('moment');
	now = now().format('YY/MM/DD HH:mm:ss');
			
	var jobJSON = {};
	
	var arrPath = strFileName.split(".");
  	var sFileExt = arrPath[arrPath.length - 1];
	
		
	if (existingJSON == null){
		jobJSON = { 'report' : [{ 'processStage': appName.toString(), 'jobName': processingJobName.toString(), 'startJob': '', 'stopJob': '', 'jobMessage': strJobMessage, 'jobMessageReason': strJobMessageReason, 'reel' : [{ 'reelName': processingReelName.toString(), 'startReel': '', 'stopReel': '', 'reelMessage': strReelMessage, 'reelMessageReason': strReelMessageReason,  'file' : [{ 'fileName': strFileName.toString(), 'fileExtension': sFileExt.toString(), 'startFile': '', 'stopFile': '', 'productionCode': strProdCode.toString(), 'cameraCode': strCameraCode.toString(), 'fileSize': strFileSize.toString(), 'duration': strDuration.toString(), 'codec': strCodec.toString(), 'height': strHeight.toString(), 'width': strWidth.toString(), 'fps': strFPS.toString(), 'aspectRatio': strAspectRatio.toString(), 'fileMessage': strFileMessage, 'fileMessageReason': strFileMessageReason, 'machine': machineName.toString()  }] , }] }] };
	}else{
		jobJSON = existingJSON;
	}	
	
		for (var a=0; a < jobJSON.report.length; a++){
			if (jobJSON.report[a].processStage == appName.toString()){
				intApp = a;
				a = jobJSON.report.length + 10;
			}
		}
		if (intApp == -1){
			//add entry for this app:
			writeLogToScreen("Add entry in JSON for App", 0);
			
			jobJSON.report.push({ 'processStage': appName.toString(),'jobName': processingJobName.toString(), 'startJob': '', 'stopJob': '', 'jobMessage': strJobMessage, 'jobMessageReason': strJobMessageReason, 'reel' : [{ 'reelName': processingReelName.toString(), 'startReel': '', 'stopReel': '', 'reelMessage': strReelMessage, 'reelMessageReason': strReelMessageReason,  'file' : [{ 'fileName': strFileName.toString(), 'fileExtension': sFileExt.toString(), 'startFile': '', 'stopFile': '', 'productionCode': strProdCode.toString(), 'cameraCode': strCameraCode.toString(), 'fileSize': strFileSize.toString(), 'duration': strDuration.toString(), 'codec': strCodec.toString(), 'height': strHeight.toString(), 'width': strWidth.toString(), 'fps': strFPS.toString(), 'aspectRatio': strAspectRatio.toString(), 'fileMessage': strFileMessage, 'fileMessageReason': strFileMessageReason, 'machine': machineName.toString() }], }] } );
			
			intApp = jobJSON.report.length - 1;
		}
		
		//else{
			//app exists - check if reel exists:
			
			//writeLogToScreen("Checking if Reel exists", 0);
			
			for (var x=0; x < jobJSON.report[intApp].reel.length; x++){
				if (jobJSON.report[intApp].reel[x].reelName == processingReelName){
					intReel = x;
					x = jobJSON.report[intApp].reel.length +10;
				}
			}
		
			if (intReel == -1){
			
				writeLogToScreen("Add entry in JSON for Reel", 0);
				
				jobJSON.report[intApp].reel.push({ 'reelName': processingReelName.toString(), 'startReel': '', 'stopReel': '', 'reelMessage': strReelMessage, 'reelMessageReason': strReelMessageReason,  'file' : [{ 'fileName': strFileName.toString(), 'fileExtension': sFileExt.toString(), 'startFile': '', 'stopFile': '', 'productionCode': strProdCode.toString(), 'cameraCode': strCameraCode.toString(), 'fileSize': strFileSize.toString(), 'duration': strDuration.toString(), 'codec': strCodec.toString(), 'height': strHeight.toString(), 'width': strWidth.toString(), 'fps': strFPS.toString(), 'aspectRatio': strAspectRatio.toString(), 'fileMessage': strFileMessage, 'fileMessageReason': strFileMessageReason, 'machine': machineName.toString() }], } );
				
				intReel = jobJSON.report[intApp].reel.length - 1;
				intFile = 0;
			}else{
				//reel found so look for the file:
				
				//writeLogToScreen("Checking if File exists", 0);
				
				for (var x=0; x < jobJSON.report[intApp].reel[intReel].file.length; x++){
					if (jobJSON.report[intApp].reel[intReel].file[x].fileName == strFileName){
						
						intFile = x;
						x = jobJSON.report[intApp].reel[intReel].file.length +10;
					}
				}
				
				if (intFile == -1){
					writeLogToScreen("Add entry in JSON for File", 0);
				
					jobJSON.report[intApp].reel[intReel].file.push({ 'fileName': strFileName.toString(), 'fileExtension': sFileExt.toString(), 'startFile': '', 'stopFile': '', 'productionCode': strProdCode.toString(), 'cameraCode': strCameraCode.toString(), 'fileSize': strFileSize.toString(), 'duration': strDuration.toString(), 'codec': strCodec.toString(), 'height': strHeight.toString(), 'width': strWidth.toString(), 'fps': strFPS.toString(), 'aspectRatio': strAspectRatio.toString(), 'fileMessage': strFileMessage, 'fileMessageReason': strFileMessageReason, 'machine': machineName.toString() } );
					
					intFile = jobJSON.report[intApp].reel[intReel].file.length - 1;
				}
			}	
		//}
		
		if (blnFileStart == true){
			if (jobJSON.report[intApp].reel[intReel].startReel == ''){
				jobJSON.report[intApp].reel[intReel].startReel = now.toString();
			}
			if (jobJSON.report[intApp].startJob == ''){
				jobJSON.report[intApp].startJob = now.toString();
			}
			
			jobJSON.report[intApp].reel[intReel].file[intFile].startFile = now.toString();
		}
		
		if (blnJobStop == true){
			jobJSON.report[intApp].stopJob = now.toString();
		}
		if (blnReelStop == true){
			jobJSON.report[intApp].reel[intReel].stopReel = now.toString();
		}
		if (blnFileStop == true){
			jobJSON.report[intApp].reel[intReel].file[intFile].stopFile = now.toString();
		}
		
		if (strJobMessage.toString() != ""){
			jobJSON.report[intApp].jobMessage = strJobMessage.toString();
			jobJSON.report[intApp].jobMessageReason = strJobMessageReason.toString();
		}
		
		if (strFileMessage.toString() != ""){
			jobJSON.report[intApp].reel[intReel].file[intFile].fileMessage = strFileMessage.toString();
			jobJSON.report[intApp].reel[intReel].file[intFile].fileMessageReason = strFileMessageReason.toString();
		}
		
		if (strJobMessageReason.toString().toLowerCase() == "error"){
			var strJFM = strFileMessage.toString();
			if (strJFM != ""){
				strJFM = strFileMessage.toString() + ". " + strJobMessage.toString();
			}else{
				strJFM = strJobMessage.toString();
			}
			jobJSON.report[intApp].reel[intReel].file[intFile].fileMessage = strFileMessage.toString() + ". " + strJobMessage.toString();
			jobJSON.report[intApp].reel[intReel].file[intFile].fileMessageReason = "ERROR";
		}
		
		if (strReelMessage.toString() != ""){
			jobJSON.report[intApp].reel[intReel].reelMessageReason = strReelMessageReason.toString();
			jobJSON.report[intApp].reel[intReel].reelMessage = strReelMessage.toString();
		}
		
		jobJSON.report[intApp].reel[intReel].file[intFile].machine = machineName.toString();
		
		
		//go through and check all end times for all files in reel and all reels in job to set end time for reels and jobs
		
		try{
			fs.truncateSync(jsonFileName, 0);
			fs.writeFileSync(jsonFileName, JSON.stringify(jobJSON));
		}catch(e){
			fs.appendFileSync(jsonFileName, JSON.stringify(jobJSON));
		}
    		
	}catch(e){
		writeLogToScreen("Caught creating JSON File: " + e.message, 1);
	}	
}

function productionCode(jobName){
	//var jobNameSplit = jobName.split("_");
	return ""
}

function cameraCode(jobName){
	// var jobNameSplit = jobName.split("_");
	// writeLogToScreen("Get camera short code: " + jobNameSplit[2], 0);
									
	// return jobNameSplit[2].toUpperCase();
	return ""
}